from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.urls import reverse

from .models import Post, Tag

"""
Миксины определяют общие моменты для наших вьюх,
от этого миксина мы будем наследовать только то, что используется общего в наших вьюхах
  """


class ObjectDetailMixin:
    model = None
    template = None

    def get(self, request, slug):
        """Метод get обрабатывает гет запросы.
        self.model.__name__.lower() вернет нам название нашей модель из models.py в нижнем регистре
        """
        obj = get_object_or_404(self.model, slug__iexact=slug)
        return render(request, self.template, context={self.model.__name__.lower(): obj,
                                                       'admin_object': obj, 'detail': True,
                                                       }
                      )


class ObjectCreateFormMixin:
    form_model = None
    template = None

    def get(self, request):
        form = self.form_model()
        return render(request, self.template, context={"form": form})

    def post(self, request):
        bound_form = self.form_model(request.POST)
        if bound_form.is_valid():
            new_obj = bound_form.save()
            return redirect(new_obj)
        return render(request, self.template, context={'form': bound_form})


class ObjectUpdateFormMixin:
    model = None  # Post or Tag
    form_model = None  # TagForm or PostForm
    template = None  # расположение шаблона

    def get(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        bound_form = self.form_model(instance=obj)
        return render(request, self.template, context={'form': bound_form, self.model.__name__.lower(): obj})

    def post(self, request, slug):
        """
        Нужно получить новые данные от пользователя,
        используем словарь request.POST
        """
        obj = self.model.objects.get(slug__iexact=slug)
        bound_form = self.form_model(request.POST, instance=obj)
        # затем проводим валидацию
        if bound_form.is_valid():
            new_obj = bound_form.save()
            return redirect(new_obj)
        return render(request, self.template, context={'form': bound_form, self.model.__name__.lower(): obj})


class ObjectDeleteFormMixin:
    model = None
    template = None
    redirect_url = None

    def get(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        return render(request, self.template, context={self.model.__name__.lower(): obj})

    def post(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        obj.delete()
        return redirect(reverse(self.redirect_url))  # перенаправляем на страницу после удаления
