from django.urls import path

from .views import posts_list, PostCreate, PostDetail, PostDelete, PostUpdate
from .views import tags_list, TagCreate, TagDetail, TagDelete, TagUpdate

# name='posts_list_url' имя создается чтобы можно было в проекте ссылаться на него
# post/<>/ --  <> означают что те символы которые идут после post/ это именованная группа символов
# и нужно присвоить этой группе ИМЯ в нашем случае это <slug> из Post, str: явно указывает что это строка(аннотация).
urlpatterns = [
    path("", posts_list, name="posts_list_url"),
    path("post/create/", PostCreate.as_view(), name="post_create_url"),
    path("post/<str:slug>/", PostDetail.as_view(), name="post_detail_url"),
    path("post/<str:slug>/update/", PostUpdate.as_view(), name="post_update_url"),
    path("post/<str:slug>/delete/", PostDelete.as_view(), name="post_delete_url"),

    path("tags/", tags_list, name="tags_list_url"),
    path("tag/create/", TagCreate.as_view(), name="tag_create_url"),
    path("tag/<str:slug>/", TagDetail.as_view(), name="tag_detail_url"),
    path("tag/<str:slug>/update/", TagUpdate.as_view(), name="tag_update_url"),
    path("tag/<str:slug>/delete/", TagDelete.as_view(), name="tag_delete_url"),
]
