from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q

from .models import Post, Tag
from .utils import ObjectDetailMixin, ObjectCreateFormMixin, ObjectUpdateFormMixin, ObjectDeleteFormMixin
from .forms import TagForm, PostForm


# Create your views here.
# процесс наполнения шаблона называется рендеринг

def posts_list(request):
    search_engine = request.GET.get("search", "")  # поиск по старнице
    if search_engine:
        posts = Post.objects.filter(Q(title__icontains=search_engine) | Q(body__icontains=search_engine))
    else:
        posts = Post.objects.all()

    paginator = Paginator(posts, 5)  # колличество постов на странице 2
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)
    # Часть логики отвечающая за кнопки далее и назад на странице
    is_paginated = page.has_other_pages()  # если есть страницы то True, нет страницы False.
    if page.has_previous():
        prev_url = f"?page={page.previous_page_number()}"
    else:
        prev_url = ""
    if page.has_next():
        next_url = f"?page={page.next_page_number()}"
    else:
        next_url = ""

    context = {
        'page_object': page,
        'is_paginated': is_paginated,
        'next_url': next_url,
        'prev_url': prev_url,
    }

    return render(request, 'blog/index.html', context=context)


class PostDetail(ObjectDetailMixin, View):
    """Метод get определён в миксине """
    model = Post
    template = 'blog/post_detail.html'


class PostCreate(LoginRequiredMixin, ObjectCreateFormMixin, View):
    form_model = PostForm
    template = 'blog/post_create.html'
    raise_exception = True


class PostUpdate(LoginRequiredMixin, ObjectUpdateFormMixin, View):
    model = Post
    form_model = PostForm
    template = 'blog/post_update.html'
    raise_exception = True


class PostDelete(LoginRequiredMixin, ObjectDeleteFormMixin, View):
    model = Post
    template = 'blog/post_delete.html'
    redirect_url = "posts_list_url"
    raise_exception = True


class TagDetail(ObjectDetailMixin, View):
    model = Tag
    template = 'blog/tag_detail.html'


class TagCreate(LoginRequiredMixin, ObjectCreateFormMixin, View):
    form_model = TagForm
    template = 'blog/tag_create.html'
    raise_exception = True


class TagUpdate(LoginRequiredMixin, ObjectUpdateFormMixin, View):
    model = Tag
    form_model = TagForm
    template = 'blog/tag_update.html'
    raise_exception = True


class TagDelete(LoginRequiredMixin, ObjectDeleteFormMixin, View):
    model = Tag
    template = 'blog/tag_delete.html'
    redirect_url = 'tags_list_url'  # перенаправления на страницу с тегами после удаления
    raise_exception = True


def tags_list(request):
    tags = Tag.objects.all()
    return render(request, 'blog/tags_list.html', context={'tags': tags})
