from django import forms
from django.core.exceptions import ValidationError

from .models import Tag, Post


class TagForm(forms.ModelForm):
    """
    Класс Form от которого мы наследуем наши свойства,
    для каждого совего поля генерирует соответсвующий html тэг.
    В терминалогии django они называются виджеты, в данном случает это будут поля input.
    Также Класс Form проводит валидацию и очистку введеных данных у него есть соответсвующие методы.
            title = forms.CharField(max_length=50)
            slug = forms.CharField(max_length=50)

            # переопределение <input class="form-control"> в нашей форме.
            title.widget.attrs.update({'class': 'form-control'})
            slug.widget.attrs.update({'class': 'form-control'})
    При ипользовании ModelForm не нужно использовать метод save:
        def save(self):
        В cleaned_data хранит только проверенные данных,
        используем в целях безопасности приложения.

        new_tag = Tag.objects.create(title=self.cleaned_data['title'],
                                     slug=self.cleaned_data['slug'],
                                     )
        return new_tag
    У ModelForm есть свой метод save.

    Если вместо class Form ипользовать ModelForm,
    который связывает между собой форму и модель которой эта форма соответсвует.
    При таком ипользовании наш TagForm будет более универсальнее.
    """

    class Meta:
        """Связывает наш Tag и форму"""
        model = Tag
        fields = ['title', 'slug']  # плохой тон указывать "__all__"

        # переопределяем наши формы ввода данных на странице tag_create
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def clean_slug(self):
        """clean_ соглашение django, если делаем clean метод для своего поля(кастомный метод),
        то этот метод должен начинаться с clean_, далее указываем поле формы к которому он должен относиться
        'clean_slug, clean_title'.
        """
        new_slug = self.cleaned_data['slug'].lower()  # self.cleaned_data.get('slug')

        if new_slug == 'create':
            raise ValidationError("Slug may not be 'Create'")
        if Tag.objects.filter(slug__iexact=new_slug).count():
            raise ValidationError(f'Данный slug уже есть: {new_slug}')

        return new_slug


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'slug', 'body', 'tags']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
            'body': forms.Textarea(attrs={'class': 'form-control'}),
            'tags': forms.SelectMultiple(attrs={'class': 'form-control'}),

        }

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()
        if new_slug == 'create':
            raise ValidationError("Slug may not be 'Create'")
        return new_slug
