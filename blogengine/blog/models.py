from time import time
from django.db import models
from django.shortcuts import reverse
from django.utils.text import slugify


# Create your models here.
def gen_slug(s):
    new_slug = slugify(s, allow_unicode=True)
    return f"{new_slug}-{int(time())}"


class Post(models.Model):
    title = models.CharField(max_length=150, db_index=True)
    #  SlugField с более специфичными валидаторами, позволяет ипользовать буквы, цифры, в обоих регистрах,
    #  дефисы и нижнее подчеркивание, все остальные знаки валидатор не позволит использовать
    # unique автоматом индексирует, поэтому не ставим db_index
    slug = models.SlugField(max_length=150, blank=True, unique=True)
    # blank=True означает что может быть пустым
    body = models.TextField(blank=True, db_index=True)
    tags = models.ManyToManyField('Tag', blank=True, related_name='posts')
    # свойство auto_now_add говорит нам, что DateTimeField будет заполняться при записи в базу данных, текущей датой
    data_pub = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        """
        Будет возвращать ссылку на конкретный объект, экземпляра класса Post
        Это называется url реверсл.
        Без него пришлось бы писать в шаблоне(см.index.html) вот так {% url 'post_detail_url' slug=post_detail.slug %}
        С ним {{ post.get_absolute_url }}
        """
        return reverse('post_detail_url', kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('post_update_url', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('post_delete_url', kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = gen_slug(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        """Отвечает за вывод информации об объекте"""
        return f"{self.title}"

    class Meta:
        """
        Сортирует посты в нашем случает по дате,
        если "-date_pub" -> новые посты вверху.
        """
        ordering = ['-data_pub']
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'


class Tag(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, unique=True)

    def get_absolute_url(self):
        return reverse('tag_detail_url', kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('tag_update_url', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('tag_delete_url', kwargs={"slug": self.slug})

    def __str__(self):
        return f"{self.title}"

    class Meta:
        ordering = ["title"]
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'
