from django.shortcuts import redirect


def redirect_blog(request):
    """
    Редирект бывает двух видов постояный, 301(permanent=True),
    и временый 302(по умолчанию)"""
    return redirect('posts_list_url', permanent=True)
